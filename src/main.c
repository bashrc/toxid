/*
 * Copyright (C) 2015-2016
 * Bob Mottram <bob@freedombone.net>
 *
 * A command to print your tox ID to the console
 * Some functions are from ToxBot https://github.com/JFreegman/ToxBot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <tox/tox.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>

#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <tox/tox.h>
#include <tox/toxav.h>
#include <tox/toxencryptsave.h>

static int detect_tox_data_file(char * search, char * username)
{
    char commandstr[512], line[512];
    char tempfile[512];
    FILE * fp;
    int i, startpos, endpos, retval;

    username[0] = 0;

    sprintf(tempfile,"%s","/tmp/toxidsearch");
    sprintf(commandstr, "if [ -f %s ]; then\nrm -f %s\nfi", tempfile, tempfile);
    retval = system(commandstr);
    sprintf(commandstr, "ls %s > %s", search, tempfile);
    retval = system(commandstr);
    fp = fopen(tempfile, "r");
    if (fp) {
        while (!feof(fp)) {
            if (fgets(line , 511 , fp) != NULL) {
                if (line == NULL) continue;
                for (endpos = strlen(line)-1; endpos > 0; endpos--) {
                    if (line[endpos]=='.') break;
                }
                for (startpos=endpos; startpos>0; startpos--) {
                    if (line[startpos]=='/') break;
                }
                if (startpos>0) startpos++;
                for (i = startpos; i < endpos; i++) {
                    username[i-startpos] = line[i];
                }
                username[i-startpos]=0;
            }
        }
        fclose(fp);
    }
    sprintf(commandstr, "rm -f %s", tempfile);
    retval = system(commandstr);
    return retval;
}

off_t file_size(const char *path)
{
    struct stat st;
    if (stat(path, &st) == -1)
        return 0;
    return st.st_size;
}

static Tox *load_tox(struct Tox_Options *options, char *path)
{
    FILE *fp = fopen(path, "rb");
    Tox *m = NULL;
    if (fp == NULL) {
        TOX_ERR_NEW err;
        m = tox_new(options, &err);
        if (err != TOX_ERR_NEW_OK) {
            fprintf(stderr, "tox_new failed with error %d\n", err);
            return NULL;
        }
        return m;
    }
    off_t data_len = file_size(path);
    if (data_len == 0) {
        fclose(fp);
        return NULL;
    }
    char data[data_len];
    if (fread(data, sizeof(data), 1, fp) != 1) {
        fclose(fp);
        return NULL;
    }
    TOX_ERR_NEW err;
    options->savedata_type = TOX_SAVEDATA_TYPE_TOX_SAVE;
    options->savedata_data = (uint8_t *) data;
    options->savedata_length = data_len;
    m = tox_new(options, &err);
    if (err != TOX_ERR_NEW_OK) {
        fprintf(stderr, "tox_new failed with error %d\n", err);
        return NULL;
    }
    fclose(fp);
    return m;
}

int tox_data_file_exists(char * data_file)
{
    FILE * fp;

    fp = fopen(data_file, "r");
    if (fp) {
        fclose(fp);
        return 1;
    }
    return 0;
}

Tox *create_tox(char * savedata_filename)
{
    Tox *tox;

    struct Tox_Options options;

    tox_options_default(&options);

    FILE *f = fopen(savedata_filename, "rb");
    if (f) {
        fseek(f, 0, SEEK_END);
        long fsize = ftell(f);
        fseek(f, 0, SEEK_SET);

        char *savedata = malloc(fsize);

        if (fread(savedata, fsize, 1, f) > 0) {
        }
        fclose(f);

        options.savedata_type = TOX_SAVEDATA_TYPE_TOX_SAVE;
        options.savedata_data = (uint8_t *)savedata;
        options.savedata_length = fsize;

        tox = tox_new(&options, NULL);

        free(savedata);
    } else {
        tox = tox_new(&options, NULL);
    }

    return tox;
}

void update_savedata_file(const Tox *tox,
                          char * filename)
{
    char savedata_tmp_filename[256];
    size_t size = tox_get_savedata_size(tox);
    char *savedata = malloc(size);

    tox_get_savedata(tox, (uint8_t *)savedata);

    sprintf(savedata_tmp_filename,"%s.tmp", filename);
    FILE *f = fopen(savedata_tmp_filename, "wb");
    if (f) {
        fwrite(savedata, size, 1, f);
        fclose(f);
    }

    rename(savedata_tmp_filename, filename);
    free(savedata);
}

int create_new_user(char * os_username, char * tox_username)
{
    char filename[256];
    char commandstr[256];
    Tox *tox = create_tox(tox_username);
    int retval;

    tox_self_set_name(tox, (const uint8_t *)tox_username, strlen(tox_username), NULL);

    /* create the directory if needed */
    sprintf(commandstr,"mkdir -p /home/%s/.config/tox", os_username);
    retval = system(commandstr);

    sprintf(filename, "/home/%s/.config/tox/%s.tox", os_username, tox_username);
    update_savedata_file(tox, filename);
    tox_kill(tox);
    return retval;
}

void get_tox_name(char * data_file, char * tox_username)
{
    Tox *tox = create_tox(data_file);
    tox_self_get_name(tox, (uint8_t *)tox_username);
    size_t length = tox_self_get_name_size(tox);
    tox_username[length]=0;
    tox_kill(tox);
}

void set_tox_name(char * data_file, char * tox_username)
{
    Tox *tox = create_tox(data_file);
    tox_self_set_name(tox, (const uint8_t *)tox_username, strlen(tox_username), NULL);
    update_savedata_file(tox, data_file);
    tox_kill(tox);
}

int main(int argc, char* argv[])
{
    char data_file[256], address[TOX_ADDRESS_SIZE];
    char set_tox_username[256], tox_username[256], username[256], new_username[256];
    struct Tox_Options tox_opts;
    Tox * tox;
    int i;
    char search[256];
    int show_user=0;
    int new_user=0;
    int set_user=0;

    /* get the user's home directory */
    struct passwd *pw = getpwuid(getuid());
    const char *homedir =  pw->pw_dir;

    /* default location of the tox data file */
    sprintf(data_file, "%s/.config/tox/data", homedir);

    set_tox_username[0] = 0;
    tox_username[0] = 0;
    username[0] = 0;
    new_username[0] = 0;

    /* parse the command line */
    for (i = 1; i < argc; i++) {
        if (i < argc-1) {
            if ((strcmp(argv[i], "--setuser") == 0) ||
                (strcmp(argv[i], "-=set") == 0)) {
                sprintf(set_tox_username, "%s", argv[i+1]);
                set_user = 1;
                i++;
            }
            if ((strcmp(argv[i], "--tox") == 0) ||
                (strcmp(argv[i], "-t") == 0)) {
                sprintf(tox_username, "%s", argv[i+1]);
                i++;
            }
            if ((strcmp(argv[i], "--user") == 0) ||
                (strcmp(argv[i], "-u") == 0)) {
                sprintf(username, "%s", argv[i+1]);
                i++;
            }
            if ((strcmp(argv[i], "--new") == 0) ||
                (strcmp(argv[i], "-n") == 0)) {
                sprintf(new_username, "%s", argv[i+1]);
                new_user=1;
                i++;
            }
        }
        if ((strcmp(argv[i], "--showuser") == 0) ||
            (strcmp(argv[i], "--show") == 0) ||
            (strcmp(argv[i], "-s") == 0)) {
            show_user = 1;
        }
    }

    /* create a new user */
    if (new_user==1) {
        if (username[0] != 0) {
            if (strlen(new_username) > 0) {
                create_new_user(username, new_username);
                return 0;
            }
            else {
                printf("Specify the tox user with --user\n");
                return 6272;
            }
        }
        printf("Specify the operating system user with --user\n");
        return 6245;
    }

    /* path for the tox user data file */
    if (username[0] != 0) {
        if (tox_username[0] != 0) {
            sprintf(data_file, "/home/%s/.config/tox/%s.tox",
                    username, tox_username);
        }
        else {
            sprintf(search, "/home/%s/.config/tox/*.tox", username);
            detect_tox_data_file(search, tox_username);
            if (tox_username[0] != 0) {
                sprintf(data_file, "/home/%s/.config/tox/%s.tox", username, tox_username);
            }
            else {
                sprintf(data_file, "/home/%s/.config/tox/data", username);
            }
        }
    }
    else {
        if (tox_username[0] != 0) {
            sprintf(data_file, "%s/.config/tox/%s.tox",
                    homedir, tox_username);
        }
        else {
            sprintf(search, "%s/.config/tox/*.tox", homedir);
            detect_tox_data_file(search, tox_username);
            if (tox_username[0] != 0) {
                sprintf(data_file, "%s/.config/tox/%s.tox", homedir, tox_username);
            }
            else {
                sprintf(data_file, "%s/.config/tox/data", homedir);
            }
        }
    }

    if (tox_data_file_exists(data_file) == 0) return 0;

    if (set_user == 1) {
        set_tox_name(data_file, set_tox_username);
        return 0;
    }

    if (show_user == 1) {
        get_tox_name(data_file, tox_username);
        printf("%s\n", tox_username);
        return 0;
    }

    /* some default options */
    memset(&tox_opts, 0, sizeof(struct Tox_Options));
    tox_options_default(&tox_opts);

    /* load */
    tox = load_tox(&tox_opts, data_file);
    if (tox == NULL) {
        printf("Failed to load or initialise\n");
        return 1;
    }

    tox_self_get_address(tox, (uint8_t *)address);
    for (i = 0; i < TOX_ADDRESS_SIZE; ++i) {
        char d[3];
        snprintf(d, sizeof(d), "%02X", address[i] & 0xff);
        printf("%s", d);
    }
    printf("\n");

    /* exit */
    tox_kill(tox);

    return 0;
}
